const https = require('https');

// REST API呼び出し
const getRestAPI = (reqParams) => {
    return new Promise(function (resolve, reject) {
        req = https.request(reqParams, function (response) {
            /*
            console.info('headers:', response.headers);
            console.info('statusCode:', response.statusCode);
            */
            var data = '';

            //another chunk of data has been recieved, so append it to `str`
            response.on('data', function (chunk) {
                data += chunk;
            });

            //the whole response has been recieved, so we just print it out here
            response.on('end', function () {
                if (response.statusCode == 200) {
                    resolve(JSON.parse(data));
                } else {
                    console.error('error', data);
                    reject(data);
                }
            });
        });
        req.on('error', function (error) {
            console.error('error', error);
            reject(error);
        });
        req.end();
    });
}

// CoinCheckレート取得
const getRateCoinCheck = (coin) => {
    let reqParams = {
        host: 'coincheck.com',
        port: 443,
        path: `/api/rate/${coin.code}_jpy`,
        method: 'get',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    return new Promise((resolve, reject) => {
        getRestAPI(reqParams).then((data) => {
            resolve(Number(data.rate));
        }, (error) => {
            reject(error);
        });
    });
};

// Zaifレート取得
const getRateZaif = (coin) => {
    let reqParams = {
        host: 'api.zaif.jp',
        port: 443,
        path: `/api/1/last_price/${coin.code}_jpy`,
        method: 'get',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    return new Promise((resolve, reject) => {
        getRestAPI(reqParams).then((data) => {
            resolve(Number(data.last_price));
        }, (error) => {
            reject(error);
        });
    });
};

// Binance 全価格取得
const getBinanceAllPrices = () => {
    let reqParams = {
        host: 'api.binance.com',
        port: 443,
        path: `/api/v1/ticker/allPrices`,
        method: 'get',
        headers: {
            'Content-Type': 'application/json'
        }
    };

    return new Promise((resolve, reject) => {
        getRestAPI(reqParams).then((data) => {
            resolve(data);
        }, (error) => {
            reject(error);
        });
    });
};

// レート取得方法定義
const rateTypes = {
    COINCHECK: 'COINCHECK',
    ZAIF: 'ZAIF',
    BINANCE_BTC: 'BINANCE_BTC'
};

// コイン定義
const COINS = {
    'btc': {
        name: 'ビットコイン',
        alias: ['ビット'],
        code: 'btc',
        order: 1,
        type: rateTypes.COINCHECK
    },
    'eth': {
        name: 'イーサリアム',
        alias: ['イーサ'],
        code: 'eth',
        order: 2,
        type: rateTypes.COINCHECK
    },
    'etc': {
        name: 'イーサリアムクラシック',
        alias: ['イークラ', 'イーサクラシック'],
        code: 'etc',
        order: 3,
        type: rateTypes.COINCHECK
    },
    'lsk': {
        name: 'リスク',
        alias: [],
        code: 'lsk',
        order: 4,
        type: rateTypes.COINCHECK
    },
    'fct': {
        name: 'ファクトム',
        alias: [],
        code: 'fct',
        order: 5,
        type: rateTypes.COINCHECK
    },
    'xmr': {
        name: 'モネロ',
        alias: [],
        code: 'xmr',
        order: 6,
        type: rateTypes.COINCHECK
    },
    'rep': {
        name: 'オーガー',
        alias: [],
        code: 'rep',
        order: 7,
        type: rateTypes.COINCHECK
    },
    'xrp': {
        name: 'リップル',
        alias: [],
        code: 'xrp',
        order: 8,
        type: rateTypes.COINCHECK
    },
    'zec': {
        name: 'ゼットキャッシュ',
        alias: ['ゼット'],
        code: 'zec',
        order: 9,
        type: rateTypes.COINCHECK
    },
    'xem': {
        name: 'ネム',
        alias: [],
        code: 'xem',
        order: 10,
        type: rateTypes.COINCHECK
    },
    'ltc': {
        name: 'ライトコイン',
        alias: ['ライト'],
        code: 'ltc',
        order: 11,
        type: rateTypes.COINCHECK
    },
    'dash': {
        name: 'ダッシュ',
        alias: [],
        code: 'dash',
        order: 12,
        type: rateTypes.COINCHECK
    },
    'bch': {
        name: 'ビットコインキャッシュ',
        alias: ['ビッチ'],
        code: 'bch',
        order: 13,
        type: rateTypes.COINCHECK
    },
    'mona': {
        name: 'モナコイン',
        alias: ['モナ', 'モナコ'],
        code: 'mona',
        order: 14,
        type: rateTypes.ZAIF
    },
    'ada': {
        name: 'エーディーエー',
        alias: ['エイダ', 'エーダ'],
        code: 'ada',
        order: 15,
        type: rateTypes.BINANCE_BTC,
        binanceSymbol: 'ADABTC'
    },
    'appc': {
        name: 'エーピーピーシー',
        alias: ['アップシー'],
        code: 'appc',
        order: 16,
        type: rateTypes.BINANCE_BTC,
        binanceSymbol: 'APPCBTC'
    },
    'bnb': {
        name: 'ビーエヌビー',
        alias: [''],
        code: 'bnb',
        order: 17,
        type: rateTypes.BINANCE_BTC,
        binanceSymbol: 'BNBBTC'
    },
    /*
    'ctr': {
        name: 'シーティーアール',
        alias: [''],
        code: 'ctr',
        order: 18,
        type: rateTypes.BINANCE_BTC,
        binanceSymbol: 'CTRBTC'
    },
    */
    'knc': {
        name: 'ケイエヌシー',
        alias: [''],
        code: 'knc',
        order: 19,
        type: rateTypes.BINANCE_BTC,
        binanceSymbol: 'KNCBTC'
    },
    'lend': {
        name: 'エルイーエヌディー',
        alias: ['レンド'],
        code: 'lend',
        order: 20,
        type: rateTypes.BINANCE_BTC,
        binanceSymbol: 'LENDBTC'
    },
    'mth': {
        name: 'エムティーエイチ',
        alias: [''],
        code: 'mth',
        order: 21,
        type: rateTypes.BINANCE_BTC,
        binanceSymbol: 'MTHBTC'
    },
    'neo': {
        name: 'エヌイーオー',
        alias: ['ネオ'],
        code: 'neo',
        order: 22,
        type: rateTypes.BINANCE_BTC,
        binanceSymbol: 'NEOBTC'
    },
    'snt': {
        name: 'エスエヌティー',
        alias: [''],
        code: 'snt',
        order: 23,
        type: rateTypes.BINANCE_BTC,
        binanceSymbol: 'SNTBTC'
    },
    'trig': {
        name: 'ティーアールアイジー',
        alias: ['トリガー'],
        code: 'trig',
        order: 24,
        type: rateTypes.BINANCE_BTC,
        binanceSymbol: 'TRIGBTC'
    },
    'trx': {
        name: 'ティーアールエックス',
        alias: ['トロン'],
        code: 'trx',
        order: 25,
        type: rateTypes.BINANCE_BTC,
        binanceSymbol: 'TRXBTC'
    },
    'xlm': {
        name: 'エックスエルエム',
        alias: [],
        code: 'xlm',
        order: 26,
        type: rateTypes.BINANCE_BTC,
        binanceSymbol: 'XLMBTC'
    },
    'xvg': {
        name: 'エックスブイジー',
        alias: [],
        code: 'xvg',
        order: 27,
        type: rateTypes.BINANCE_BTC,
        binanceSymbol: 'XVGBTC'
    }
};
exports.COINS = COINS;

class VirtualCurrency {
    constructor() {
        this.rates = {};
        this.binanceAllPrices = null;
    }

    _getBinanceAllPrices() {
        return new Promise((resolve, reject) => {
            if (this.binanceAllPrices) {
                //console.log(`_getBinanceAllPrices() from cache`);
                resolve(this.binanceAllPrices);
                return;
            }

            getBinanceAllPrices().then((data) => {
                this.binanceAllPrices = data;
                resolve(this.binanceAllPrices);
            }, (error) => {
                reject(error);
            });
        });
    }

    _getRate(coin) {
        return new Promise((resolve, reject) => {
            const resolveRate = (rate) => {
                this.rates[coin.code] = rate;
                resolve(rate);
            };

            switch (coin.type) {
                case rateTypes.COINCHECK:   // CoinCheck
                    getRateCoinCheck(coin).then((rate) => {
                        console.log(`_getRate(${coin.code}) = ${rate}`);
                        resolveRate(rate);
                    }, (error) => {
                        reject(error);
                    });
                    break;

                case rateTypes.ZAIF:    // ZAIF
                    getRateZaif(coin).then((rate) => {
                        console.log(`_getRate(${coin.code}) = ${rate}`);
                        resolveRate(rate);
                    }, (error) => {
                        reject(error);
                    });
                    break;

                case rateTypes.BINANCE_BTC: // Binance (BTC建て)
                    this.getRate(COINS.btc).then((btcRate) => {
                        this._getBinanceAllPrices().then((allPrices) => {

                            if (!allPrices) {
                                reject(new Error(`Binance All Prices is null`));
                                return;
                            }

                            let p = allPrices.find((s) => {
                                return s.symbol === coin.binanceSymbol;
                            });

                            if (p) {
                                let rate = btcRate * p.price;
                                console.log(`_getRate(${coin.code}) = ${rate}(${btcRate} * ${p.price})`);
                                resolveRate(rate);
                            } else {
                                reject(new Error(`invalid binanceSynbol[${coin.binanceSymbol}]`));
                            }
                        }, (error) => {
                            reject(error);
                        });
                    }, (error) => {
                        reject(error);
                    });

                default:
                    reject(new Error(`invalid rateTypes`));
            }
        });
    }

    getRate(coin) {
        return new Promise((resolve, reject) => {
            if (this.rates[coin.code]) {
                //console.log(`getRate(${coin.code}) from cache`);
                resolve(this.rates[coin.code]);
            }

            this._getRate(coin).then((rate) => {
                resolve(rate);
            }, (error) => {
                resolve(0);
            });
        });
    }

    getAllRates() {
        return new Promise((resolve, reject) => {
            const getAll = () => {
                // 残りを並列で取る
                Promise.all(
                    Object.keys(COINS).map((k) => {
                        const coin = COINS[k];
                        return this.getRate(coin);
                    })
                ).then(() => {
                    resolve(this.rates);
                });
            };
            
            // BTCのレートとBinanceのallPriceは先に取ってキャッシュする
            Promise.all([
                this.getRate(COINS.btc),
                this._getBinanceAllPrices()
            ]).then(() => {
                getAll();
            }, (error) => {
                console.error(`GET BTC/Binance AllPrice failed`);
                getAll();
            });

        });
    }
}

exports.VirtualCurrency = VirtualCurrency;