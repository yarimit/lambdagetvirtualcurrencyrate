const AWS = require('aws-sdk');
const VC = require('virtual-currency.js');

function digit02d(d) {
    return ('0' + d).slice(-2);
}

exports.handler = (event, context, callback) => {
    const virtualCurrency = new VC.VirtualCurrency();
    
    virtualCurrency.getAllRates().then((rates) => {
        const dt = new Date();
        const key = `${dt.getFullYear()}-${digit02d(dt.getMonth()+1)}-${digit02d(dt.getDate())}T${digit02d(dt.getHours())}`;
        const documentClient = new AWS.DynamoDB.DocumentClient();
        documentClient.put({
            "TableName" : process.env.DYNAMODB_TABLE,
            "Item": {
                "date" : key,
                "rates": rates
            }
        }, function(err, data) {
            if(err) {
                console.error(`Failed. ${err}`);
                callback(null, `Failed. ${err}`);
            } else {
                console.log(`Success. ${JSON.stringify(rates)}`);
                callback(null, `Success. ${JSON.stringify(rates)}`);
            }
        });
    }, (err) => {
        console.error(`Failed. ${err}`);
        callback(null, `Failed. ${err}`);
    });
};
